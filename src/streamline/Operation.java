package streamline;

public interface Operation {

	Object call(Object msg);
	
}
